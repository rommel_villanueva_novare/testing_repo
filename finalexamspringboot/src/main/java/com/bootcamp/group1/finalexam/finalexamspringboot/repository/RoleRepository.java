package com.bootcamp.group1.finalexam.finalexamspringboot.repository;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Role;
import com.bootcamp.group1.finalexam.finalexamspringboot.entity.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
