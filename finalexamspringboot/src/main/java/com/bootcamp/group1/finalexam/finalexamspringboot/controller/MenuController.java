package com.bootcamp.group1.finalexam.finalexamspringboot.controller;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Menu;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.MenuServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class MenuController {

    private MenuServiceImpl menuService;

    @Autowired
    private MenuController(MenuServiceImpl menuService) {
        this.menuService = menuService;
    }

    @GetMapping("restaurant/{type}")
    public List<Menu> getMenu(@PathVariable("type") String type) {
        return menuService.getAllMenu(type);
    }

    @PostMapping("restaurant")
    public String addMenu(@RequestBody Menu menu) {
        menuService.addMenu(menu);
        return "Successfully added a Menu";
    }

    //test
    @PostMapping("restaurant/batch")
    public String addBatchMenu(@RequestBody Menu[] menu) {
        List<Menu> menuList = Arrays.asList(menu);
        System.out.println("");
        menuService.addBatchMenu(menuList);
        return "Successfully added a batch of Menus";
    }

    @PutMapping("restaurant/{id}")
    public void updateMenu(@PathVariable("id") String id, @RequestBody Menu menu) {
        menu.setId(id);
        menuService.addMenu(menu);
    }

    @DeleteMapping("restaurant/{id}")
    public void deleteMenu(@PathVariable("id") String id) {
        menuService.deleteMenu(id);
    }
}
