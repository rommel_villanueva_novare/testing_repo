package com.bootcamp.group1.finalexam.finalexamspringboot.service.interfaces;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Menu;

import java.util.List;

public interface IMenuService {
    List<Menu> getAllMenu(String cuisineType);
    void addMenu(Menu menu);
    void deleteMenu(String id);
    void addBatchMenu(List<Menu> menuList);
}
