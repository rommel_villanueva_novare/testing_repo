package com.bootcamp.group1.finalexam.finalexamspringboot.service.impl;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Review;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.ReviewRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.interfaces.IReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReviewServiceImpl implements IReviewService {
    private ReviewRepository reviewRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public String reviewOrder(Review review) {
        reviewRepository.save(review);
        return "Success";
    }
}
