package com.bootcamp.group1.finalexam.finalexamspringboot.service.interfaces;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Review;

public interface IReviewService {
    String reviewOrder(Review review);
}
