package com.bootcamp.group1.finalexam.finalexamspringboot.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "review")
@Data
public class Review {
    @Id
    private String id;
    private String orderId;
    private String customerId;
    private String feedBack;
}
