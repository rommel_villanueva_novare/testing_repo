package com.bootcamp.group1.finalexam.finalexamspringboot.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

@Document(collection = "menu")
@Data
public class Menu {
    @Id
    private String id;
    private String name;
    private String cuisineType;
    private double price;
}
