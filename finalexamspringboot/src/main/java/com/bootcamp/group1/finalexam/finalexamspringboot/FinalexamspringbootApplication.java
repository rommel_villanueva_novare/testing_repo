package com.bootcamp.group1.finalexam.finalexamspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalexamspringbootApplication {
    public static void main(String[] args) {
        SpringApplication.run(FinalexamspringbootApplication.class, args);
    }
}
