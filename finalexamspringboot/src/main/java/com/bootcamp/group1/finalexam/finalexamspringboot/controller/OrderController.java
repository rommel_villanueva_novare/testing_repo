package com.bootcamp.group1.finalexam.finalexamspringboot.controller;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Order;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {
    private OrderServiceImpl orderService;

    @Autowired
    private OrderController(OrderServiceImpl orderService) {
        this.orderService = orderService;
    }

    @GetMapping("order/{id}")
    public Order getOrder(@PathVariable("id") String id) {
        return orderService.getAllOrder(id);
    }

    @PostMapping("order")
    public String addOrder(@RequestBody Order order) {
        orderService.addOrder(order);
        return "New Order Added";
    }

    @PutMapping("order/{id}")
    public void updateOrder(@PathVariable("id") String id, @RequestBody Order order) {
        order.setId(id);
        orderService.addOrder(order);
    }
}
