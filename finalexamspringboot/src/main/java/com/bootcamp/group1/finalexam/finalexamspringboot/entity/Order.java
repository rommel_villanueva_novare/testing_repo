package com.bootcamp.group1.finalexam.finalexamspringboot.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Document(collection = "order")
@Data
public class Order {
    @Id
    private String id;
    private String orderDate;
    private String orderStatus;
    private List<Menu> menu;
}
