package com.bootcamp.group1.finalexam.finalexamspringboot.entity;

public enum RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}
