package com.bootcamp.group1.finalexam.finalexamspringboot.service.interfaces;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Order;

public interface IOrderService {
    Order getAllOrder(String id);
    void addOrder(Order order);
}
