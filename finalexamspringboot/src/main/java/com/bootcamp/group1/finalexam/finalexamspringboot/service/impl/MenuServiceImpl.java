package com.bootcamp.group1.finalexam.finalexamspringboot.service.impl;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Menu;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.MenuRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.interfaces.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements IMenuService {

    private MenuRepository menuRepository;

    @Autowired
    public MenuServiceImpl(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    public List<Menu> getAllMenu(String cuisineType) {
        return menuRepository.findAllByCuisineType(cuisineType);
    }

    @Override
    public void addMenu(Menu menu) {
        menuRepository.save(menu);
    }

    @Override
    public void deleteMenu(String id) {
        menuRepository.deleteById(id);
    }

    @Override
    public void addBatchMenu(List<Menu> menuList) {
        for (Menu menu : menuList) {
            menuRepository.save(menu);
        }
    }
}
