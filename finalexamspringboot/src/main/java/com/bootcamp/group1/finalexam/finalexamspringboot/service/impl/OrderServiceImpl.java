package com.bootcamp.group1.finalexam.finalexamspringboot.service.impl;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Menu;
import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Order;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.MenuRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.OrderRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.interfaces.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements IOrderService {

    private OrderRepository orderRepository;

    private MenuRepository menuRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, MenuRepository menuRepository) {
        this.orderRepository = orderRepository;
        this.menuRepository = menuRepository;
    }

    @Override
    public Order getAllOrder(String id) {
        return orderRepository.findById(id).orElse(new Order());
    }

    @Override
    public void addOrder(Order order) {
        List<Menu> menuOrderList = new ArrayList<>();
        for (Menu menu: order.getMenu()) {
            if(menuRepository.findById(menu.getId()).orElse(new Menu()).getPrice() != 0) {
                menuOrderList.add(menuRepository.findById(menu.getId()).orElse(new Menu()));
            }
        }
        order.setMenu(menuOrderList);
        orderRepository.save(order);
    }
}
