package com.bootcamp.group1.finalexam.finalexamspringboot.repository;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Review;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends MongoRepository<Review, String> {
}
