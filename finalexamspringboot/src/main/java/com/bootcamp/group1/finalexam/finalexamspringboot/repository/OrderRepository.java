package com.bootcamp.group1.finalexam.finalexamspringboot.repository;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends MongoRepository<Order, String> {
}
