package com.bootcamp.group1.finalexam.finalexamspringboot.service.impl;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Review;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.ReviewRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReviewServiceImplTest {
    @Mock
    private ReviewRepository reviewRepository;

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Test
    public void review_reviewOrderReview_equalsSuccess() {
        Review review = new Review();
        Mockito.when(reviewRepository.save(review)).thenReturn(review);
        Assert.assertEquals("Success", reviewService.reviewOrder(review));
    }
}
