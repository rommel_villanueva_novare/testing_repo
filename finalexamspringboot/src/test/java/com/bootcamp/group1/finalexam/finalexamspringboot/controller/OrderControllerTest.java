package com.bootcamp.group1.finalexam.finalexamspringboot.controller;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Order;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.OrderServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderControllerTest {
    @InjectMocks
    private OrderController orderController;

    @Mock
    private OrderServiceImpl orderService;

    @Mock
    private Order mockedOrder;

    @Test
    public void getOrder_getOrderMatchingId_equalsTrue() {

        when(orderService.getAllOrder(any())).thenReturn(mockedOrder);
        assertEquals(mockedOrder, orderController.getOrder(any()));
    }

    @Test
    public void addOrder_addOrderToDatabase_equalsTrue() {
        orderController.addOrder(mockedOrder);
        verify(orderService, times(1)).addOrder(mockedOrder);
    }

    @Test
    public void updateOrder_updateOrderInDatabase_equalsTrue() {
        orderController.updateOrder(anyString(), mockedOrder);
        verify(orderService, times(1)).addOrder(mockedOrder);
    }
}
