package com.bootcamp.group1.finalexam.finalexamspringboot.entity;

import com.bootcamp.group1.finalexam.finalexamspringboot.repository.MenuRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.MenuServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class MenuTest {
    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuServiceImpl menuService;

    @Test
    public void menu_menuEntity_initializeObject_equalsObjectInitialized() {
        Menu menu = new Menu();
        Mockito.when(menuRepository.save(menu)).thenAnswer((Answer) i -> {

            menu.setId("1");
            menu.setName("name");
            menu.setCuisineType("type");
            menu.setPrice(5.5);

            return null;
        });
        menuService.addMenu(menu);
        Assert.assertEquals("1", menu.getId());
        Assert.assertEquals("name", menu.getName());
        Assert.assertEquals("type", menu.getCuisineType());
        Assert.assertEquals(5.5, menu.getPrice(), 0.2);
    }
}
