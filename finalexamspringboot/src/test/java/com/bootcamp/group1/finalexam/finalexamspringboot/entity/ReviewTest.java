package com.bootcamp.group1.finalexam.finalexamspringboot.entity;

import com.bootcamp.group1.finalexam.finalexamspringboot.repository.ReviewRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.ReviewServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class ReviewTest {

    @Mock
    private ReviewRepository reviewRepository;

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Test
    public void review_reviewOrderReview_setIdOne_equalsOne() {
        Review review = new Review();
        Mockito.when(reviewRepository.save(review)).thenAnswer((Answer) i -> {

            review.setId("1");

            return null;
        });
        reviewService.reviewOrder(review);
        Assert.assertEquals("1", review.getId());
    }

    @Test
    public void review_reviewOrderReview_setOrderIdOne_equalsOne() {
        Review review = new Review();
        Mockito.when(reviewRepository.save(review)).thenAnswer((Answer) i -> {

            review.setOrderId("1");

            return null;
        });
        reviewService.reviewOrder(review);
        Assert.assertEquals("1", review.getOrderId());
    }

    @Test
    public void review_reviewOrderReview_setCustomerIdOne_equalsOne() {
        Review review = new Review();
        Mockito.when(reviewRepository.save(review)).thenAnswer((Answer) i -> {

            review.setCustomerId("1");

            return null;
        });
        reviewService.reviewOrder(review);
        Assert.assertEquals("1", review.getCustomerId());
    }

    @Test
    public void review_reviewOrderReview_setFeedBackSample_equalsSample() {
        Review review = new Review();
        Mockito.when(reviewRepository.save(review)).thenAnswer((Answer) i -> {

            review.setFeedBack("sample");

            return null;
        });
        reviewService.reviewOrder(review);
        Assert.assertEquals("sample", review.getFeedBack());

    }
}
