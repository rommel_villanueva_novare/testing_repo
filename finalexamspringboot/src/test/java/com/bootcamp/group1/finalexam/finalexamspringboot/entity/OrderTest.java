package com.bootcamp.group1.finalexam.finalexamspringboot.entity;

import com.bootcamp.group1.finalexam.finalexamspringboot.repository.MenuRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.OrderRepository;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.OrderServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//@RunWith(MockitoJUnitRunner.class)
public class OrderTest {
    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    Menu menu;

//    @Test
//    public void order_orderEntity_initializeObject_equalsObjectInitialized() {
//        List<Menu> menuList = Arrays.asList(menu);
//        Order order = new Order();
//        Mockito.when(orderRepository.save(order)).thenAnswer((Answer) i -> {
//
//            order.setId("1");
//            order.setOrderDate("2019-10-10");
//            order.setOrderStatus("Cancelled");
//            order.setMenu(menu);
//
//            return null;
//        });
//        orderService.addOrder(order);
//        Assert.assertEquals("1", order.getId());
//        Assert.assertEquals("2019-10-10", order.getOrderDate());
//        Assert.assertEquals("Cancelled", order.getOrderStatus());
//        Assert.assertEquals(menuList, order.getMenu());
//    }
}
