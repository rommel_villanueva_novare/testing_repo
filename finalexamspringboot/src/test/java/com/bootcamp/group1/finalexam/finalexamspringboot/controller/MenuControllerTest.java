package com.bootcamp.group1.finalexam.finalexamspringboot.controller;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Menu;
import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.MenuServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MenuControllerTest {

    @InjectMocks
    private MenuController menuController;

    @Mock
    private MenuServiceImpl menuService;

    @Mock
    private Menu mockedMenu;

    @Test
    public void getMenu_getMenuMatchingCuisine_equalsTrue() {
        List<Menu> listOfMenu = Arrays.asList(mockedMenu, mockedMenu, mockedMenu);

        when(menuService.getAllMenu(any())).thenReturn(listOfMenu);
        assertEquals(listOfMenu, menuController.getMenu(any()));
    }

    @Test
    public void addMenu_addMenuToDatabase_equalsTrue() {
        menuController.addMenu(mockedMenu);
        verify(menuService, times(1)).addMenu(mockedMenu);
    }

    @Test
    public void updateMenu_updateMenuInDatabase_equalsTrue() {
        menuController.updateMenu(anyString(), mockedMenu);
        verify(menuService, times(1)).addMenu(mockedMenu);
    }

    @Test
    public void deleteMenu_deleteMenuInDatabase_equalsTrue() {
        menuController.deleteMenu(anyString());
        verify(menuService, times(1)).deleteMenu(anyString());
    }

    @Test
    public void addBatchMenu_addBatchMenuToDatabase_equalsTrue() {
        List<Menu> listOfMenu = Arrays.asList(mockedMenu, mockedMenu, mockedMenu);
        Menu[] arrayOfMenu = {mockedMenu, mockedMenu, mockedMenu};
        menuController.addBatchMenu(arrayOfMenu);
        verify(menuService, times(1)).addBatchMenu(listOfMenu);
    }
}
