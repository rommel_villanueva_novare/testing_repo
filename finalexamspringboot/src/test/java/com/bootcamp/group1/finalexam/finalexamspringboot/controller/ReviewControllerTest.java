package com.bootcamp.group1.finalexam.finalexamspringboot.controller;

import com.bootcamp.group1.finalexam.finalexamspringboot.service.impl.ReviewServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class ReviewControllerTest {


    @Mock
    private ReviewServiceImpl reviewService;

    @InjectMocks
    private ReviewController reviewController;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(reviewController).build();
    }

    @Test
    public void review_reviewOrderReview_set() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(post("/review").contentType(MediaType.APPLICATION_JSON).content("{\"id\": \"1\",\"orderId\": \"1\",\"customerId\": \"1\",\"feedBack\": \"too greasy!\"}")).andReturn().getResponse();
        Assert.assertEquals("Success Adding Order Review", response.getContentAsString());
    }
}
