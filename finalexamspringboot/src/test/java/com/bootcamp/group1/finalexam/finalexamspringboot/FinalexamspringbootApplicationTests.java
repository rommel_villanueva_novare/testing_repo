package com.bootcamp.group1.finalexam.finalexamspringboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinalexamspringbootApplicationTests {

    @Test
    public void contextLoads() {
        FinalexamspringbootApplication.main(new String[] {});
    }
}
