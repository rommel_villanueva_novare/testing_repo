package com.bootcamp.group1.finalexam.finalexamspringboot.service.impl;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Menu;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.MenuRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MenuServiceImplTest {

    @InjectMocks
    private MenuServiceImpl menuService;

    @Mock
    private MenuRepository menuRepository;

    @Mock
    private Menu mockedMenu;

    @Test
    public void getAllMenu_getMenuMatchingCuisine_equalsTrue() {
        List<Menu> listOfMenu = Arrays.asList(mockedMenu, mockedMenu, mockedMenu);

        when(menuRepository.findAllByCuisineType(any())).thenReturn(listOfMenu);
        assertEquals(listOfMenu, menuService.getAllMenu("any"));
    }

    @Test
    public void addMenu_addMenuToDatabase_equalsTrue() {
        when(menuRepository.save(mockedMenu)).thenReturn(mockedMenu);

        menuService.addMenu(mockedMenu);
        verify(menuRepository, times(1)).save(mockedMenu);
    }

    @Test
    public void deleteMenu_deleteMenuFromDatabase_equalsTrue() {
        menuService.deleteMenu(anyString());
        verify(menuRepository, times(1)).deleteById(anyString());
    }

    @Test
    public void addBatchMenu_addBatchMenuToDatabase_equalsTrue() {
        List<Menu> listOfMenu = Arrays.asList(mockedMenu, mockedMenu, mockedMenu);
        when(menuRepository.save(mockedMenu)).thenReturn(mockedMenu);

        menuService.addBatchMenu(listOfMenu);
        verify(menuRepository, times(3)).save(mockedMenu);
    }
}
