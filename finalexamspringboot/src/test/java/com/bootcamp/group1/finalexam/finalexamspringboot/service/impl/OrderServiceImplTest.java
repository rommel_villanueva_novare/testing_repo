package com.bootcamp.group1.finalexam.finalexamspringboot.service.impl;

import com.bootcamp.group1.finalexam.finalexamspringboot.entity.Order;
import com.bootcamp.group1.finalexam.finalexamspringboot.repository.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private Order mockedOrder;

    @Test
    public void getAllOrder_getOrderMatchingCuisine_equalsTrue() {

        when(orderRepository.findById(any())).thenReturn(java.util.Optional.of(mockedOrder));
        assertEquals(mockedOrder, orderService.getAllOrder("any"));
    }

    @Test
    public void addOrder_addOrderToDatabase_equalsTrue() {
        when(orderRepository.save(mockedOrder)).thenReturn(mockedOrder);

        orderService.addOrder(mockedOrder);
        verify(orderRepository, times(1)).save(mockedOrder);
    }
}
